---
id: workflow
title: Git Workflow
sidebar_label: Git Workflow
---

![](<https://wac-cdn.atlassian.com/dam/jcr:b5259cce-6245-49f2-b89b-9871f9ee3fa4/03%20(2).svg?cdnVersion=814>)

Cada projeto conta com três branches principais e algumas auxiliares. A seguir vemos as definições de cada uma.

## PRINCIPAIS

### Master

A branch master é onde fica todo o código que está em produção. Quando seu código estiver pronto, validado e for feita uma release do projeto, usaremos essa branch para fazer os merges.

### Homolog

É a branch cópia da master! Tudo que está na master deve ser refletido no ambiente de homolog.

### Develop

Todo código é mergeado primeiramente em develop. É a branch que os desenvolvedores usam para a primeira versão de um código.

## AUXILIARES

### Feature

Toda branch que agrega algo novo ao projeto é chamada de feature branch.

Criando uma feature branch: `git checkout -b feat/header`

### Hotfix/Bugfix

Quando é detectado um problema no código que está em produção esse tipo de branch é criada.
Assim que a resolução do problema é feita é realizado um PR para master/staging e develop.

Este tipo de branch é criada quando precisamos ajustar algo que está quebrado no sistema.

Criando uma hotfix branch: `git checkout -b fix/header`

### Release

A branch release é sempre criada a partir da develop quando desejamos fazer um release do produto para produção.
Quando a branch de release está pronta, ela é mergeada na develop, homolog e master.

Criando uma branch de release: `git checkout -b release/0.1.0`

## Fluxo geral de release

1. Feature branches são criadas a partir da develop
   OBS.: Lembre-se que antes de abrir um PR da sua branch o ideial é fazer um pull rebase da branch destino (Exemplo: `git pull origin develop --rebase`). Isso pode prevenir você de abrir um PR com conflitos e já coloca sua modificações a frente do resto do código que está na branch de destino
2. Assim que finalizada a feature é feito um PR para develop e é colado o link no canal do #pr-review
3. Assim que validada, sua branch é mergeada na develop (marcando a opção de squash de commits)
4. Uma branch release é criada a partir da develop
5. Assim que o release estiver pronto, é feito o merge para homolog
6. Assim que esta branch de release foi aprovada sem bugs em homolog, é mergeada também na master e develop
7. Se algum bug foi detectado na branch master é aberta uma branch hotfix a partir da master
8. Assim que o bug é resolvido, a branch de hotfix é mergeada tanto na master quanto na develop

Para saber mais sobre fluxo de branches -> https://nvie.com/posts/a-successful-git-branching-model/
