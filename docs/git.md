---
id: git
title: Introdução
sidebar_label: Introdução
---

[Git](https://git-scm.com/) é o sistema de versionamento que utilizamos aqui na Nave. Nessa seção você irá aprender as técnicas que usamos para versionar nosso código.

### Links Uteis

- [Git Explorer](https://gitexplorer.com/)
