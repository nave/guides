---
id: knex
title: Knex
sidebar_label: Knex
---

Aqui na nave utilizamos bancos relacionais na maioria dos projetos e, a fim de obter uma camada de abstração em cima dessa tecnologia, gostamos bastante de usar o pacote [`knex`](http://knexjs.org/). O knex possui diversas funcionalidades e nos permite manipular bancos SQL de diversas formas, nesse guide iremos abordar as mais utilizadas no cotidiano.

## Instalação

Para instalar o knex, devemos instalar o pacote próprio do mesmo e em conjunto o pacote do banco que iremos utilizar (na nave costumamos utilizar o PostgreSQL).

``` 
yarn add knex pg
```

## Configuração

Por padrão costumamos deixar as configurações do knex em um arquivo separado, o qual chamamos de `knexfile.js`. Neste arquivo é feito, por exemplo, a configuração da conexão com o banco:

```
import knex from 'knex'

const database = {
  client: 'pg',
  connection: process.env.DATABASE_URL
}

export default knex(database)

```

**OBS:** É uma boa prática salvar as credenciais do banco como variavéis de ambiente.


## Migrations

Utilizamos o padrão de `migrations` ou `schema migrations` a fim de obter um maior controle de todas alterações estruturais em um banco de dados relacionais. O knex possui suporte a esse padrão, veremos em seguida como se dá essa funcionalidade.

As `migrations` são conjuntos de alterações, que chamamos em inglês de `batchs`, que queiramos executar em um determinado banco. No knex, cada uma dessas `migrations` possui um arquivo único e um projeto pode possuir diversos desses arquivos. Com o objetivo de mantermos o código mais organizado, é uma boa prática deixarmos esses arquivos em um pasta separada. O caminho para esta pasta pode ser definada no nosso `knexfile.js`, nesse caso teriamos algo assim:

```
import knex from 'knex'

const database = {
  client: 'pg',
  connection: process.env.DATABASE_URL,
  migrations: {
    directory: 'caminho/para/pasta' 
  }
}

export default knex(database)

```

Em cada um desses arquivos possuímos duas funções principais, denominadas de: `up` e `down`. 
Na função de `up` codificamos as modificações necessárias que queiramos executar a fim de alterar a estrutura atual do banco. 
Já no `down` é feito o processo contrário, essa função tem como objetivo desfazer as alterações realizadas na função de `up` e retornar a estrutura antiga do banco.
Para criarmos um arquivo de migração, executamos o seguinte comando:

```

knex migrate:make nome_do_arquivo 

```

**OBS**: É uma boa prática nomearmos o arquivo com algo que simbolize o que aquela migração irá executar.

E assim será criado, na pasta configurada no `knexfile`, o seguinte arquivo:

```
.
├── migrations 
    └── TIMESTAMP_nome_do_arquivo.js
```

Esse timestamp é utilizado por padrão, a partir dele conseguimos manter as migrations ordenadas e organizadas. Dentro do arquivo criado poderemos executar scripts utilizando os métodos do próprio knex, como os de criação e remoção de tabelas no banco. Abaixo, um exemplo prático:

```
export const up = knex =>
  knex.schema
    .createTable('users', table => {
      table.uuid('id').primary()
      table.string('name').notNullable()
    })

export const down = knex =>
  knex.schema.dropTableIfExists('users')

```

Note que quando executarmos a função de `up` criaremos uma tabela de `users` com os campos de `id` e `name`. 
Na função de `down` fazemos o caminho reverso, nesse caso removemos a tabela de `users` e assim voltaremos ao estado anterior a função de `up`. 

Podemos executar as últimas `migrations` criadas da seguinte forma:

```
knex migrate:latest
```

Para voltarmos as últimas `migrations` executadas:

```
knex migrate:rollback
```


## Seeds

As `seeds` são funções que inserem dados pré-determinados a fim de popular o banco de dados. Essas informações, em sua maioria, são essencias para o bom funcionamento da aplicação.

Assim como as `migrations`, as `seeds` ficam separadas em arquivos próprios.
O caminho para esta pasta é definada também no `knexfile.js`, por exemplo:

```
import knex from 'knex'

const database = {
  client: 'pg',
  connection: process.env.DATABASE_URL,
  migrations: {
    directory: 'caminho/para/pasta' 
  },
  seeds: {
    directory: 'caminho/para/pasta' 
  }
}

export default knex(database)
```

Para criarmos os arquivos de `seed`, executamos o seguinte comando:

```
knex seed:make nome_do_arquivo
```

E assim será criado, na pasta configurada no `knexfile`, o seguinte arquivo:

```
.
├── seeds 
    └── nome_do_arquivo.js
```

Exemplo de arquivo de seed: 

```
export const seed = async knex => {
  await knex('users').insert({ name: 'usuario' })
}

```

Para executarmos os arquivos de `seed`, rodamos o seguinte comando:

```
knex seed:run
```

**OBS:** Esse comando irá rodar todas as seeds em **ordem**. Caso exista necessidade de serem executadas em uma ordem especifica, é necessário nomear os arquivos em ordem alfabética.

