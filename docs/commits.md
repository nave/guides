---
id: commits
title: Commits
sidebar_label: Commits
---

Aqui na Nave usamos [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) junto com o [git-cz](https://github.com/commitizen/cz-cli) para padronizar as mensagens em commits. Todo projeto deve ter o `git-cz` instalado!
