---
id: readme
title: Template de Readme
sidebar_label: Template de Readme
---

# Título do projeto

### Descrição

Aqui descreva em poucas palavras o projeto

### Como rodar o projeto

Descreva todos os detalhes necessários para rodar o projeto

### CI

Coloque informações relevantes sobre o CI. (qual o CI, onde está hospedado, etc)

### Postman

Coloque aqui o link do postman

## Ambiente de DEV

### Hospedagem

Coloque informações relevantes sobre a hospedagem (NÃO a conta em questão. Ex.: Hospedado na conta da AWS do cliente)

### Link do front

cole aqui o link para o front de dev

### Link do back

cole aqui o link para o back de dev

## Ambiente de HOMOLOG

### Hospedagem

Coloque informações relevantes sobre a hospedagem (NÃO a conta em questão. Ex.: Hospedado na conta da AWS do cliente)

### Link do front

cole aqui o link para o front de homologação

### Link do back

cole aqui o link para o back de homologação

## Ambiente de PRODUÇÃO

### Hospedagem

Coloque informações relevantes sobre a hospedagem (NÃO a conta em questão. Ex.: Hospedado na conta da AWS do cliente)

### Link do front

cole aqui o link para o front de produção

### Link do back

cole aqui o link para o back de produção

## Tips and Tricks

coloque aqui dicas sobre o projeto que acha relevante compartilhar para melhor entendimento no caso de um handover
