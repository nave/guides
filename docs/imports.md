---
id: imports
title: Imports
sidebar_label: Imports
---

# Padrão de imports

Primeiramente, é essencial seguir o padrão de `root import` em todo o projeto. Caso o projeto ainda não esteja com o suporte para o `root-import`, adicione [esse pugin do babel](https://github.com/entwicklerstube/babel-plugin-root-import#readme).

Para ajudar na legibilidade, é importante que seja deixado uma linha em branco separando os diferentes escopos de `import.` Imagine um arquivo que importa os seguintes tipos de escopos:

```
- Biblioteca
- Components
- Páginas
- Funções e helpers
- Estilos
- Imagens
```

### Import para projetos front-end

Para cada escopo diferente, deixe uma linha em branco para facilitar na hora que for ler aquele arquivo.
Ex:

```
import React from 'react'
import styled from 'styled-components'
import moment from 'moment'

import { Card, Button } from 'components'

import { getUserById } from 'services'
import { useDebounce } from 'hooks'

```

Para cada pasta do projeto, é necessário um arquivo `index.js` que exporta tudo de todos os arquivos, para facilitar na hora de fazer os `imports`.
Ex:
`src/services/index.js`

```
export * from './users'
export * from './posts'
```

### Import para projetos back-end

Os imports no back-end também devem separar os diferentes escopos de `import.` usando linhas em branco considerando os respectivos escopos:

```
- Bibliotecas
- Model 
- Funções e helpers
```

Para cada escopo diferente, deixe uma linha em branco para facilitar na hora que for ler aquele arquivo.
Ex:

```
import  { v4 } from 'uuid'
import  bcrypt from 'bcrypt'

import User from 'models/User'
import Role from 'models/Role'

import { getPaginationData } from 'helpers'
```

Em geral o arquivo `index.js` não existe em todas as pastas da aplicação, porém, na pasta helpers e routes ele existe, como no front-end, para facilitar na hora dos `imports`.

**src/helpers/index.js**

Cada helper deve ser exportado da seguinte maneira dentro do arquivo:

```
export * from './errors'
export * from './file'
export * from './password'
export { default as sendEmail } from './nodemailer'
```

**src/routes/index.js**

Nesse arquivo todas as rotas são centralizadas e agrupadas em um `Router()` que por sua vez adiciona o prefixo de versionamento da API. Isso resulta no `import` de todas as rotas da aplicação pelo arquivo server.js de uma vez.

```
const router = new Router()
const api = new Router()

api.use(users)
api.use(me)

router.use('/v1', api.routes())

export default router
```

### Possíveis problemas relacionados a imports
   
### Import circular

Esse é um erro comum que já ocorreu entre diversos projetos na empresa. Para contextualizar, geralmente ocorre quando dois módulos referenciam um ao outro de forma direta ou indireta. \
Como exemplificado nos tópicos acima, utilizamos o padrão de, para cada pasta de projetos, criar um arquivo index que exporta todos os outros arquivos dessa pasta. Nesse tipo de estrutura geralmente resolvemos esse bug da seguinte forma: importando o trecho de código desejado diretamente do arquivo onde este está salvo, ao inves de importar utilizando o padrão do index.js.

Para exemplificar, vamos supor a seguinte estrutura de pastas:

```
.
├── controllers
│   └── user-controller.js
├── models
│   ├── index.js
│   └── User.js
└── helpers
    ├── index.js
    ├── check-user.js
    └── error-handling.js

```

Geralmente nos arquivos de `models` utilizamos funções dos `helpers` e, algumas vezes, nos `helpers` usamos as `models`, e assim, é um caso propício a ocorrer o import circular. 
Por exemplo, se na model `User` utilizassemos funções de tratamento de erro existentes no arquivo de `error-handling` e no helper `check-user` utilizemos a mesma model `User` para criar uma função que confere no banco de dados se um usuário já existe, provavelmente teriamos um erro de import circular ao chamar esse helper no controller `user-controller`.

Caso ocorra essa situação, geralmente resolvemos da seguinte forma:

**models/User.js**
```
### ERRO CIRCULAR

import { NotFound } from 'helpers'

...
```

```
### RESOLVENDO ERRO CIRCULAR

import { NotFound } from 'helpers/error-handling'

...
```

Alguns exemplos de artigos/discussão que falam sobre situações parecidas envolvendo esse erro:

https://spin.atomicobject.com/2018/06/25/circular-dependencies-javascript/
https://gist.github.com/gund/fa244bfef5024ebe2aba72d31d565cb0