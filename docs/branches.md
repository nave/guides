---
id: branches
title: Branches
sidebar_label: Branches
---

![](https://www.atlassian.com/dam/jcr:389059a7-214c-46a3-bc52-7781b4730301/hero.svg)

`git branch`

Listar todas as ramificações no seu repositório. Isso é sinônimo de git branch --list.

`git checkout -b <branch>`

Uma outra forma de criar uma branch.

`git checkout <branch>`

Ir para a branch em questão.

`git branch -d <branch>`

Excluir a ramificação especificada. Esta é uma operação “segura” em que o Git impede que você exclua a ramificação se tiver mudanças não mescladas.

`git branch -D <branch>`

Forçar a exclusão da ramificação especificada, mesmo que ela tenha mudanças não mescladas. Este é o comando a ser usado se você quiser excluir de modo permanente todas as confirmações associadas a uma linha particular de desenvolvimento.

`git branch -m <branch>`

Renomear a ramificação atual para <branch>.

`git branch -a`

Listar todas as ramificações remotas.
