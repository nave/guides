---
id: pull-requests
title: Pull Requests
sidebar_label: Pull Requests
---

![](https://wac-cdn.atlassian.com/dam/jcr:09308632-38a3-4637-bba2-af2110629d56/07.svg?cdnVersion=810)

Um pull request é o ato de enviar melhorias para o repositório e poder [mergear](https://git-scm.com/docs/git-merge) a sua melhoria na branch destino. No fluxo que temos na empresa, se você criou uma branch `feat/header` e quer que ela vá para a develop, você precisa que seu PR passe pelas aprovações no canal do slack #pr-review.
