---
id: services
title: Services
sidebar_label: Services
---

# Padrão de services

A maior parte dos projetos consomem serviços externos. Esse guide consiste em explicar em como padronizar os services de um projeto (caso o projeto esteja rodando com esse padrão). Segue aqui um exemplo de estrutura de pastas: 

```
.
├── services
│   └── posts.js
├── components
│   ├── Button
│   └── Input
├── routes
│   ├── Home
│   └── Login
├── helpers
│   ├── auth.js
│   └── formatters.js
├── yarn.lock
├── package.json
└── node_modules

```

Nesse cenário, os services devem servir como abstração da regra de negócio do consumo dessa `API` externa. Segue um exemplo:
```javascript
// services/posts.js

import moment from 'moment'

import client from 'providers/fetchClient'

const createPost = async post => {
  
  const postInfo = {
    title: post.title,
    content: post.content,
    created_at: moment(post.created_at).format('DD/MM/YYYY')
  }

  const newPost = await client.post('/posts', postInfo)

  return newPost

}

export const postsServices = { createPost }

```

> Esse é um exemplo bem simples no qual a data de criação de um 'post' precisa ser formatada antes de ser enviada para o endpoint, então, abstraímos essa regra pra dentro do serviço tornando o nosso componente/controller mais limpo e mantendo a regra de negócio de um serviço externo controlada dentro do seu próprio escopo. 

Importante salientar também que o motivo de não tratarmos essa `Promise` diretamente dentro do serviço é porque esperamos que ela seja tratada no componente/controller, onde tu vai poder disparar diferentes tipos de estados baseado no resultado dessa requisição. Dada essa situação, poderiamos ter um componente `React` que consumisse essa serviço dessa forma:

```jsx
import React, { useState, useEffect } from 'react'

import { postsServices } from 'services/posts'

const MyComponent = () => {

  const [loading, setLoading] = useState(false)
  
  const handleSubmit = async data => {

    setLoading(true)

    try {
      await postServices.createPost(data)
      alert('eba! seu post foi criado com sucesso.')
    } catch {
      alert('ops! não conseguimos criar esse post.')
    } finally {
      setLoading(false)
    }

  }

  return (
    <form onSubmit={handleSubmit}> 
      {'random jsx here'}
    </form>
  )
}

export default MyComponent
```