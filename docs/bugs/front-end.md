---
id: front-end
title: Resolvendo Bugs no Front-end
sidebar_label: Front-end
---

O maior problema para a maioria dos desenvolvedores nem é sempre como resolver o problema, mas como encontrá-lo. Muitas vezes ficamos "batendo cabeça" debuggando o código e ainda assim não conseguimos resolver o problema.
Abaixo temos uma lista de problemas de front-end que acontecem frequentemente e suas possíveis causas, pra todo desenvolvedor que não está conseguindo resolver o maldito problema, poder tentar algumas abordagens diferentes.

## Problemas gerais

### Autoplay de videos do youtube com audio em iframe

Se a página não está focada, ele não da play devido a uma política dos browsers que não permite. Se a página estiver focada, funciona.

 A única solução até o momento é deixar o autoplay sem audio.

### Geolocalização do usuário nos navegadores firefox/safari no MacOS

O sistema operacional pode estar bloqueando os navegadores de buscar a localização do usuário. Não é uma solução a nível de código por enquanto, e o paliativo é colocar uma localização “default” pra caso ocorra o erro, ou orientar o usuário a dar permissão pelas configurações do sistema.

### Projeto em react sofrendo uma piscada nos textos quando altera um estado em algum componente pela primeira vez

As fontes importadas pelo GlobalStyles do styled-components tao sofrente re-fetch (https://github.com/styled-components/styled-components/issues/2205). A solução é importar as fontes no index.html do projeto.


## Problemas específicos do IE11

### Navbar não está ficando fixa no topo

Pode estar relacionado a propriedade `position` utilizada. Caso seja `sticky`, o ie11 não suporta essa funcionalidade e é bem provável que seja este o problema. Use `position: absolute` para contornar esse problema.