---
id: devops
title: Resolvendo Bugs na infraestrutura
sidebar_label: DevOps
---

O maior problema para a maioria dos desenvolvedores nem é sempre como resolver o problema, mas como encontrá-lo. Muitas vezes ficamos "batendo cabeça" debuggando o código e ainda assim não conseguimos resolver o problema.
Abaixo temos uma lista de problemas de front-end que acontecem frequentemente e suas possíveis causas, pra todo desenvolvedor que não está conseguindo resolver o maldito problema, poder tentar algumas abordagens diferentes.

## Aos problemas

#### Task do ECS restartando sem parar


*Primeira possível causa*

A possível causa do problema pode ser o health check do load balancer estar retardando a task. Por default, o health check verifica se a resposta da aplicação retorna o status 200, senão ela restarta pois a aplicação não está respondendo corretamente.
Isso pode acontecer pois na rota `/` das api's normalmente retornam o status 401. Modifique o endpoint do health check ou adicione como regra todos os status de 200 até 499.

*Segunda possível causa*

Se o tipo das tasks são EC2, o que pode estar acontecendo é o mapeamento das portas não estar dinâmico e a task que substituiria a antiga não estar conseguindo ser colocada na mesma porta. Verifique se o hostPort na task do ECS está como `0`, se não estiver mude para `0`. Isso fará com que o mapeamento das portas ocorra automaticamente para o elb.

#### Request com arquivos grandes no servidor não funciona, mas local sim.

A possível causa pode estar no nginx que limita o body da request em 1 MB antes de chegar na aplicação. A solução seria ajustar o limitador de tamanho da request dentro dos arquivos de configuração do nginx.