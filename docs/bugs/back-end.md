---
id: back-end
title: Resolvendo Bugs no back-end
sidebar_label: Back-end
---

O maior problema para a maioria dos desenvolvedores nem é sempre como resolver o problema, mas como encontrá-lo. Muitas vezes ficamos "batendo cabeça" debuggando o código e ainda assim não conseguimos resolver o problema.
Abaixo temos uma lista de problemas de front-end que acontecem frequentemente e suas possíveis causas, pra todo desenvolvedor que não está conseguindo resolver o maldito problema, poder tentar algumas abordagens diferentes.

## Aos problemas

